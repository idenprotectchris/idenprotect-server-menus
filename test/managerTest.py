'''
Created on 11 Jul 2017

@author: ChrisRussell
'''
import unittest
from manager.Manager import Manager
from manager.DbManager import DbManager

class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testName(self):
        pass
    
    def test_prefix(self):
        testManager = Manager("test.properties")
        testString = "test.type.fred"
        testPrefix = "test.type."
        self.assertEqual(testManager.removePrefix(testPrefix, testString), "fred")

    def test_prefix_list(self):
        testManager = Manager("test.properties")
        testDict = {'prefix.prefix1.value1': '1','prefix.prefix1.value2': '2','prefix.prefix1.value3': '3'}
        testPrefix = 'prefix.prefix1.'
        testResult = testManager.removePrefixFromSettings(testPrefix, testDict)
        self.assertEqual(testResult['value1'], '1')
    
    def test_url_stripping(self):
        testManager = DbManager("dbtest.properties")
        testManager.getSettings()
        self.assertEqual(testManager.settings[DbManager.prefix + 'port'], '3306')
        self.assertEqual(testManager.settings[DbManager.prefix + 'host'], 'db.domain.com')
        testManager.setSettings(testManager.settings)
        testManager.getSettings()
        self.assertEqual(testManager.settings[DbManager.prefix + 'port'], '3306')
        self.assertEqual(testManager.settings[DbManager.prefix + 'host'], 'db.domain.com')
        
        
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
    

        
        
    

    