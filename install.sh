#!/usr/bin/env bash
# This wizard allow user to install ApplyMobile iSPA solution in /opt folder
# 

export PARTIALLOOP="show"
export MAINLOOP='a'
export CERTSUBJECT


echo ""
echo "                                 iSPA installation WIZARD"
echo "Installation logs can be found in /tmp/ispa_log"
echo ""




# Set execution context
cd `dirname $BASH_SOURCE`

# Creating log directory in /tmp
mkdir -p /tmp/ispa_log

# Error handling
function throwErrors {
  set -e
}

function ignoreErrors {
  set +e
}

function errReport {
  echo "Error on line $1"
}

# Get value from user ( $1 - desc., $2 - example,)
function setValue {
  export DECISION="."
  while [ $DECISION ];
  do
    echo $1
    echo $2
    read RESULT
    echo "Do you want to use this value? [y]/[N]"
    read USERDECISION
    case $USERDECISION in
      y)
        unset DECISION
      ;;
      N,n,'')
      ;;
    esac
  done
}


function setVars {

  while [ $PARTIALLOOP ];
  do
  echo "Set parameters, they will be needed in the installation process:"

    echo -e "1) Nginx TLS certificate Distinguished Name: \t [$CERTSUBJECT]"



    echo -e "2) Virtualization type: \t\t\t [$VIRTOPT]"
 

    echo -e "16) Password for Java trust keystore: \t\t [$JAVAKEYSTOREPASSWD]"
    echo -e "17) Password for administrator P12 keystore \t [$P12PASSWD]"
    echo -e "18) Password for web server SSL Keystore \t [$SSLKEYSTOREPASSWD]"
  fi
  if [ $EJBCAPART = "x" ]; then
    echo -e "19) System user ejbca password: \t\t [$SYSTEMEJBCAPASSWD]"
    echo -e "20) System user identear password: \t\t [$SYSTEMISPAPASSWD]"
    echo -e "21) EJBCA Database password for user ejbcadb: \t [$EJBCADBPASSWD]"

  fi

  if [[ "$ISPAPART" == "x" || "$EJBCAPART" == "x" ]];then
    echo -e "22) EJBCA FQDN: \t\t\t\t [$EJBCAFQN]"
  fi

  echo ""
  echo "To edit parameter type its number or b to get back: [number]/[b]"
  read INSTALLDEC
  case $INSTALLDEC in
    1)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide nginx TLS cert subject with fields: C, O, OU, CN" "(e.g. /C=GB/O=ExampleOrg/OU=Information Technology/CN=ispa.example.com)"
        CERTSUBJECT=$RESULT
        TESTVAR=`echo $CERTSUBJECT | grep "/C=[a-zA-Z0-9/\.\_]*" | grep "/O=[a-zA-Z0-9/\.\_]*" | grep "/OU=[a-zA-Z0-9/\.\_]*" | grep "/CN=[a-zA-Z0-9/\.\_]*"`
        if [[ "$TESTVAR" == "" ]];then
          CERTSUBJECT="WARNING!: "$CERTSUBJECT
        fi
      fi
    ;;
    2)
      if [[ "$ISPAPART" == "x" || "$EJBCAPART" == "x" ]];then
        setValue "Provide type of virtualization" "(vmware/virtualbox/none)"
        VIRTOPT=$RESULT
        TESTVAR=`echo $VIRTOPT | grep "^vmware$" || echo $VIRTOPT | grep "^virtualbox$" || echo $VIRTOPT | grep "^none$"`
        if [[ "$TESTVAR" == "" ]];then
          VIRTOPT="WARNING!: "$VIRTOPT
        fi
      fi
    ;;
    3)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Should LDAP be enabled:" "(true/false)"
        LDAPENABLE=$RESULT
        TESTVAR=`echo $LDAPENABLE | grep "^false$" || echo $LDAPENABLE | grep "^true$"`
        if [[ "$TESTVAR" == "" ]];then
          LDAPENABLE="WARNING!: "$LDAPENABLE
        fi
      fi
    ;;
    3a)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap type:" "(e.g. real)"
        ISPALDAPTYPE=$RESULT
      fi
    ;;

    3b)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap server:" "(e.g. ldap://10.0.1.5:389)"
        ISPALDAPSERVER=$RESULT
      fi
    ;;

    3c)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap auth method:" "(e.g. simple)"
        ISPALDAPAUTHMETHOD=$RESULT
      fi
    ;;

    3d)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap auth user:" "(e.g. cn=ispa,ou=Service Accounts,dc=example,dc=com)"
        ISPALDAPAUTHUSER=$RESULT
      fi
    ;;
    3e)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap auth password:" ""
        ISPALDAPAUTHPASSWD=$RESULT
      fi
    ;;
    3f)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap search base:" "(e.g. ou=Users,dc=example,dc=com)"
        ISPALDAPSEARCHBASE=$RESULT
      fi
    ;;
    3g)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap search object class:" "(e.g. person)"
        ISPALDAPSEARCHOBJCLASS=$RESULT
      fi
    ;;
    3h)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap field user name" "(e.g. uid)"
        ISPALDAPFIELDUSER=$RESULT
      fi
    ;;
    3i)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap field first name" "(e.g. givenName)"
        ISPALDAPFIELDFIRST=$RESULT
      fi
    ;;
    3j)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap field last name" "(e.g. uid)"
        ISPALDAPFIELDLAST=$RESULT
      fi
    ;;
    3k)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap field full name" "(e.g. uid)"
        ISPALDAPFIELDFULL=$RESULT
      fi
    ;;
    3l)
      if [[ "$LDAPENABLE" == "true" ]];then
        setValue "Provide ldap field user name" "(e.g. uid)"
        ISPALDAPFIELDEMAIL=$RESULT
      fi
    ;;
    4)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Should LDAP Autoenroll be enabled" "(true/false)"
        LDAPAUTO=$RESULT
        TESTVAR=`echo $LDAPAUTO | grep "^false$" || echo $LDAPAUTO | grep "^true$"`
        if [[ "$TESTVAR" == "" ]];then
          LDAPAUTO="WARNING!: "$LDAPAUTO
        fi
      fi
    ;;

    4a)
      if [[ "$LDAPAUTO" == "true" ]];then
        setValue "Provide autoenroll cert:" "(e.g. cn=iSPA_User_Allow,ou=Group,dc=example,dc=com)"
        ISPALDAPCERTENROLL=$RESULT
      fi
    ;;

    5)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide database host:" "(e.g. 127.0.0.1)"
        ISPADBHOST=$RESULT
      fi
    ;;
    6)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide database port:" "(e.g. 3306)"
        ISPADBPORT=$RESULT
      fi
    ;;
    7)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide database username:" "(e.g. AUTH_SERVER)"
        ISPADBUSER=$RESULT
      fi
    ;;
    8)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide database password:" ""
        ISPADBPASSWD=$RESULT
      fi
    ;;
    9)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide ejbca host:" "(e.g. ispa.example.com)"
        ISPAEJBCAHOST=$RESULT
      fi
    ;;
    10)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide ejbca port:" "(e.g. 8443)"
        ISPAEJBCPORT=$RESULT
      fi
    ;;
    11)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide email gateway username:" "(e.g. admin@example.com)"
        ISPAMAILGATEWAYUSERNAME=$RESULT
      fi
    ;;
    12)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide email gateway password:" ""
        ISPAMAILGATEWAYPASSWD=$RESULT
      fi
    ;;
    13)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide email gateway host:" "(e.g. emailserver.example.com)"
        ISPAMAILGATEWAYHOST=$RESULT
      fi
    ;;
    14)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide email gateway port:" "(e.g. 25)"
        ISPAMAILGATEWAYPORT=$RESULT
      fi
    ;;
    15)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Provide email gateway from:" "(e.g. admin@example.com)"
        ISPAMAILGATEWAYFROM=$RESULT
      fi
    ;;
    # 11)
    #   if [[ "$ISPAPART" == "x" ]];then
    #     setValue "Set password for web admin user:" ""
    #     ISPAADMINPASSWD=$RESULT
    #   fi
    # ;;
    16)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Set password for Java trust keystore:" "(e.g. changeit)"
        JAVAKEYSTOREPASSWD=$RESULT
      fi
    ;;
    17)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Set password for admin P12 keystore" "(e.g. s3cR3TP445W0rD)"
        P12PASSWD=$RESULT
      fi
    ;;
    18)
      if [[ "$ISPAPART" == "x" ]];then
        setValue "Set password for web server SSL keystore" "(e.g. Th4Ti5S3Cr3T)"
        SSLKEYSTOREPASSWD=$RESULT
      fi
    ;;
    19)
      if [[ "$EJBCAPART" == "x" ]];then
        setValue "Set password for ejbca system user:" ""
        SYSTEMEJBCAPASSWD=$RESULT
      fi
    ;;
    20)
      if [[ "$EJBCAPART" == "x" ]];then
        setValue "Set password for identear system user:" ""
        SYSTEMISPAPASSWD=$RESULT
      fi
    ;;
    21)
      if [[ "$EJBCAPART" == "x" ]];then
        setValue "Set password for database user ejbca-user:" ""
        EJBCADBPASSWD=$RESULT
      fi
    ;;
    22)
      if [[ "$ISPAPART" == "x" || "$EJBCAPART" == "x" ]];then
        setValue "Set FQDN for EJBCA. _WARNING_: Sensitive parameter for EJBCA. Make sure you set the correct value." "(e.g. ispa.example.com)"
        EJBCAFQN=$RESULT
      fi
    ;;
    b)
      unset PARTIALLOOP
    ;;
    *)
     echo "--Invalid option--"
    ;;
  esac
  done
}

function installSolution {

 # Prepare temporary directory for scripts
  if [ -e `pwd`/wizard.sh ] && [ -d `pwd`/scripts ] && [ -d `pwd`/files ] ; then
    echo "Init actions:"
    mkdir -p /tmp/files
    echo "mkdir /tmp/files - done"
    mkdir -p /tmp/scripts
    echo "mkdir /tmp/scripts - done"
    mkdir -p /tmp/templates
    echo "mkdir /tmp/templates - done"
    cp -r `pwd`/scripts/* /tmp/scripts
    cp -r `pwd`/files/* /tmp/files
    if [ -d `pwd`/templates ]; then
    cp -r `pwd`/templates/* /tmp/templates
    mkdir -p /opt/identear
    # Ilkka K additions for standalone installer
    subscription-manager config --rhsm.manage_repos=0
    echo "Disable online repos and subscription-manager - Done"
    cp -r $(pwd)/files/packages/ /opt/identear
    cp /tmp/files/idenprotect-server.repo /etc/yum.repos.d
    yum clean all
    yum repolist all 
    echo "Move Packages to Directory and update local repo - Done"


    else
      echo "Error: Config templates not found"
      exit 1
    fi
  else
    echo "Error: "
    exit 1
  fi

 # Amending and running the scripts
  cd /tmp

  cat templates/sslfront.temp | sed 's|{{ cert }}|'"$CERTSUBJECT"'|g' > scripts/sslfront.sh
  cat templates/ejbca.temp.sh | sed 's|{{ domain }}|'"$EJBCAFQN"'|g' > scripts/ejbca.sh
  sed 's|{{ ejbcaPass }}|'"$SYSTEMEJBCAPASSWD"'|g' -i scripts/ejbca.sh
  sed 's|{{ identearPass }}|'"$SYSTEMISPAPASSWD"'|g' -i scripts/ejbca.sh

 # ispa sql
  cat templates/ispa_create.temp | sed 's|{{ dbUserName }}|'"$ISPADBUSER"'|g' > files/ispa_create.sql
  sed 's|{{ dbPassword }}|'"$ISPADBPASSWD"'|g' -i files/ispa_create.sql
 # ejbca sql
  cat templates/ejbca_create.temp | sed 's|{{ ejbcaDbPasswd }}|'"$EJBCADBPASSWD"'|g' > files/ejbca_create.sql
 # database properties
  cat templates/database.temp | sed 's|{{ ejbcaDbPasswd }}|'"$EJBCADBPASSWD"'|g' > files/database.properties
 # web properties - Password for java trust keystore
  cat templates/web_properties.temp | sed 's|{{ changeit }}|'"$JAVAKEYSTOREPASSWD"'|g' > files/web.properties
  sed 's|{{ adminP12Passwd }}|'"$P12PASSWD"'|g' -i files/web.properties
  sed 's|{{ webSSLKeystore }}|'"$SSLKEYSTOREPASSWD"'|g' -i files/web.properties


  cd scripts
  if [ $ISPAPART = "x" ]; then
    #throwErrors
    sh sslfront.sh 2>&1 | tee /tmp/ispa_log/sslfront.sh.log
    #ignoreErrors
  fi
  if [ $EJBCAPART = "x" ]; then
    throwErrors
    semodule -i ../files/mariadb.pp
    sh ejbca_db.sh 2>&1 | tee /tmp/ispa_log/ejbca_db.sh.log
    ignoreErrors
  fi
  if [ $ISPAPART = "x" ]; then
    sh ispa_db.sh 2>&1 | tee /tmp/ispa_log/ispa_db.sh.log
  fi
  if [ $EJBCAPART = "x" ]; then
    throwErrors
    sh ejbca.sh 2>&1 | tee /tmp/ispa_log/ejbca.sh.log
    ignoreErrors
  fi

  # Case statement
  
  
  
 ;s
 ls
 :q
 
  case $VIRTOPT in
    vmware)
      sh vmware.sh 2>&1 | tee /tmp/ispa_log/vmware.sh.log
      ;;
    virtualbox)
      sh virtualbox.sh 2>&1 | tee /tmp/ispa_log/virtualbox.sh.log
      ;;
    *)
      echo "Virtualization not used"
      ;;
  esac

  # Basic installation completed - Configuration below
  if [ $ISPAPART = "x" ]; then
    cd /tmp/templates
    cat nginx.temp > /etc/nginx/nginx.conf
    echo "Nginx config updated (/etc/nginx/nginx.conf)"
    mkdir -p /opt/identear
    cd /opt/identear
    cp /tmp/files/*.jar .
    cp /tmp/templates/env.temp env.sh
    cp /tmp/templates/ispa.temp ispa.sh
    mkdir good-ca-adapters
    cp -r /tmp/files/apache-tomcat-8.5.11/ ./good-ca-adapters/
    echo "Files moved to /opt/identear"

    # env
    sed 's/{{ domain }}/'"$ISPAEJBCAHOST"'/g' -i env.sh
    sed 's/{{ port }}/'"$ISPAEJBCPORT"'/g' -i env.sh
    sed 's/{{ dbHost }}/'"$ISPADBHOST"'/g' -i env.sh
    sed 's/{{ dbPort }}/'"$ISPADBPORT"'/g' -i env.sh
    sed 's/{{ dbUserName }}/'"$ISPADBUSER"'/g' -i env.sh
    sed 's/{{ dbPassword }}/'"$ISPADBPASSWD"'/g' -i env.sh

    # ispa
    sed 's|{{ domain }}|'"$EJBCAFQN"'|g' -i ispa.sh
    sed 's/{{ domain }}/'"$ISPAEJBCAHOST"'/g' -i ispa.sh
    sed 's/{{ port }}/'"$ISPAEJBCPORT"'/g' -i ispa.sh
    sed 's/{{ dbHost }}/'"$ISPADBHOST"'/g' -i ispa.sh
    sed 's/{{ dbPort }}/'"$ISPADBPORT"'/g' -i ispa.sh
    sed 's/{{ dbUserName }}/'"$ISPADBUSER"'/g' -i ispa.sh
    sed 's/{{ dbPassword }}/'"$ISPADBPASSWD"'/g' -i ispa.sh
    sed 's/{{ changeit }}/'"$JAVAKEYSTOREPASSWD"'/g' -i ispa.sh
    sed 's/{{ adminP12Passwd }}/'"$P12PASSWD"'/g' -i ispa.sh

    if [[ "$LDAPENABLE" = "yes" ]]; then
      sed 's/{{ ldapEnable }}/true/g' -i ispa.sh
      sed 's/# {{ ldapType }}/-Dldap.type='"$ISPALDAPTYPE"'/g' -i ispa.sh
      sed 's/# {{ ldapServer }}/-Dldap.server='"$ISPALDAPSERVER"'/g' -i ispa.sh
      sed 's/# {{ ldapAuthMethod }}/-Dldap.auth.method='"$ISPALDAPAUTHMETHOD"'/g' -i ispa.sh
      sed 's/# {{ ldapAuthUser }}/-Dldap.auth.user="'"$ISPALDAPAUTHUSER"'"/g' -i ispa.sh
      sed 's/# {{ ldapAuthPass }}/-Dldap.auth.pass="'"$ISPALDAPAUTHPASSWD"'"/g' -i ispa.sh
      sed 's/# {{ ldapSearchBase }}/-Dldap.search.base="'"$ISPALDAPSEARCHBASE"'"/g' -i ispa.sh
      sed 's/# {{ ldapSearchObjClass }}/-Dldap.search.objectClass='"$ISPALDAPSEARCHOBJCLASS"'/g' -i ispa.sh
      sed 's/# {{ ldapFieldUser }}/-Dldap.field.user.name='"$ISPALDAPFIELDUSER"'/g' -i ispa.sh
      sed 's/# {{ ldapFieldFirst }}/-Dldap.field.first.name='"$ISPALDAPFIELDFIRST"'/g' -i ispa.sh
      sed 's/# {{ ldapFieldLast }}/-Dldap.field.last.name='"$ISPALDAPFIELDLAST"'/g' -i ispa.sh
      sed 's/# {{ ldapFieldFull }}/-Dldap.field.full.name='"$ISPALDAPFIELDFULL"'/g' -i ispa.sh
      sed 's/# {{ ldapFieldEmail }}/-Dldap.field.email='"$ISPALDAPFIELDEMAIL"'/g' -i ispa.sh
    else
      sed 's/{{ ldapEnable }}/false/g' -i ispa.sh
      sed '/# {{ ldapType }} \\/d' -i ispa.sh
      sed '/# {{ ldapServer }} \\/d' -i ispa.sh
      sed '/# {{ ldapAuthMethod }} \\/d' -i ispa.sh
      sed '/# {{ ldapAuthUser }} \\/d' -i ispa.sh
      sed '/# {{ ldapAuthPass }} \\/d' -i ispa.sh
      sed '/# {{ ldapSearchBase }} \\/d' -i ispa.sh
      sed '/# {{ ldapSearchObjClass }} \\/d' -i ispa.sh
      sed '/# {{ ldapFieldUser }} \\/d' -i ispa.sh
      sed '/# {{ ldapFieldFirst }} \\/d' -i ispa.sh
      sed '/# {{ ldapFieldLast }} \\/d' -i ispa.sh
      sed '/# {{ ldapFieldFull }} \\/d' -i ispa.sh
      sed '/# {{ ldapFieldEmail }} \\/d' -i ispa.sh
    fi

    if [[ "$LDAPAUTO" = "yes" ]]; then
      sed 's/{{ ldapAutoEnroll }}/true/g' -i ispa.sh
      # echo "Provide autoenroll cert: (ex. cn=iSPA_User_Allow,ou=Group,dc=example,dc=com)"
      # read CERTENROLL
      sed 's/# {{ ldapCertEnroll }}/'"$ISPALDAPCERTENROLL"'/g' -i ispa.sh
    else
      sed 's/{{ ldapAutoEnroll }}/false/g' -i ispa.sh
      sed '/# {{ ldapCertEnroll }} \\/d' -i ispa.sh
    fi

    # Email setup
    sed 's/# {{ EmailGatewayUsername }}/-Demail.gateway.username='"$ISPAMAILGATEWAYUSERNAME"'/g' -i ispa.sh
    sed 's/# {{ EmailGatewayPass }}/-Demail.gateway.password='"$ISPAMAILGATEWAYPASSWD"'/g' -i ispa.sh
    sed 's|# {{ EmailGatewayHost }}|-Demail.gateway.host='"$ISPAMAILGATEWAYHOST"'|g' -i ispa.sh
    sed 's/# {{ EmailGatewayPort }}/-Demail.gateway.port='"$ISPAMAILGATEWAYPORT"'/g' -i ispa.sh
    sed 's/# {{ EmailGatewayFrom }}/-Demail.gateway.from='"$ISPAMAILGATEWAYFROM"'/g' -i ispa.sh

    if [[ "$ISPAMAILGATEWAYUSERNAME" = '' ]] &&  [[ "$ISPAMAILGATEWAYPASSWD" = '' ]] &&
    [[ "$ISPAMAILGATEWAYHOST" = '' ]] && [[ "$ISPAMAILGATEWAYPORT" = '' ]] && [[ "$ISPAMAILGATEWAYFROM" = '' ]];then
     sed '/# {{ EmailGatewayUsername }}\\/d' -i ispa.sh
     sed '/# {{ EmailGatewayPass }}\\/d' -i ispa.sh
     sed '/# {{ EmailGatewayHost }}\\/d' -i ispa.sh
     sed '/# {{ EmailGatewayPort }}\\/d' -i ispa.sh
     sed '/# {{ EmailGatewayFrom }}\\/d' -i ispa.sh
    fi




    # service nginx restart
    # SELinux
    setenforce 0

    sh /tmp/scripts/hardening.sh && echo "Hardening - OK"
    chcon --reference=/etc/nginx/fastcgi_params /etc/nginx/nginx.conf
    chcon --reference=/etc/fail2ban/jail.conf /etc/fail2ban/jail.local
    chcon --reference=/etc/nginx/fastcgi_params /var/certs/server.crt
    chcon --reference=/etc/nginx/fastcgi_params /var/certs/server.key
    chcon --reference /etc/nginx/fastcgi_params /var/certs/dhparams.pem

    yum -y install policycoreutils-python
    yum -y install setroubleshoot
    export LC_ALL=en_US.UTF-8
    cd /tmp/templates
    #grep nginx /var/log/audit/audit.log | audit2allow -M auditPol2
    #semodule -i /tmp/templates/auditPol2.pp
    setsebool -P httpd_can_network_connect 1
    setsebool -P httpd_read_user_content 1
    setsebool -P httpd_enable_homedirs 1
    setsebool -P allow_ypbind 1
    semodule -i /tmp/files/nginx.pp
    # semodule -i /tmp/templates/ispaPol.pp
    setenforce 1
    echo "SELinux configured"


    # # Import DB IDENTEAR
    # mysql -e "CREATE DATABASE IDENTEAR"
    # echo $dbPassword > mysql -u $dbUserName -p IDENTEAR < /tmp/templates/db.sql
    #
    #
    # #Passwd change
    # echo "Setting new password for web admin"
    # yum -y install gcc libffi-devel python-devel python-setuptools

    # cd /tmp/files/python_lib/bcrypt-2.0.0
    # python setup.py install
    #
    #
    #
    # ISPAADMINPASSWD=`python -c "import bcrypt;passwd=b'$ISPAADMINPASSWD';hash=bcrypt.hashpw(passwd,bcrypt.gensalt(10));print hash;"`
    # mysql -e "use IDENTEAR; UPDATE WEBSITE_USERS SET PASSWORD = '$ISPAADMINPASSWD' WHERE USERNAME='ADMIN';"
    #
    #
    # yum -y remove gcc libffi-devel python-devel python-setuptools
    echo "Installation completed"

    echo "#########################################################################"
    echo "#                             WARNING                                   #"
    echo "#########################################################################"
    echo "This installation is _insecure_ to change this state please reset        "
    echo "administrator password for web application as fast as possible.          "
    echo "To do it please login at admin account -> open tab site -> reset password"
    echo "for ADMIN user.                                                          "
    echo "#########################################################################"

    service nginx restart
  fi
  exit 0

}


function main {
  while [ $MAINLOOP ];
    do
      echo "Select action:"
      echo "1) Choose components which should be installed"
      echo "2) Enter organization-specific iSPA server setup parameters"
      echo "3) INSTALL"
      echo ""
      echo "To select installation options type its number or q to quit: [1]/[2]/[3]/[q]"
      read MAINDEC
      case $MAINDEC in
        1)
          choseComponents
        ;;
        2)
          setVars
        ;;
        3)
        echo ""
          if [[ "$CERTSUBJECT" == "" && "$ISPAPART" == "x" ]]; then
            echo "! Please set Nginx certificate subject"
          elif [[ "$VIRTOPT" == "" && "$ISPAPART" == "x" ]] || [[ "$VIRTOPT" == "" && "$EJBCAPART" == "x" ]]; then
            echo "! Please set virtualization type"
          elif [[ ! "$LDAPENABLE" == "false" ]] && [[ "$ISPAPART" == "x" ]] && [[ "$ISPALDAPTYPE" == ""  || "$ISPALDAPSERVER" == "" ||
           "$ISPALDAPAUTHMETHOD" == ""  ||  "$ISPALDAPAUTHUSER" == ""  ||  "$ISPALDAPAUTHPASSWD" == "" ||
           "$ISPALDAPSEARCHBASE" == ""  ||  "$ISPALDAPSEARCHOBJCLASS" == ""  ||  "$ISPALDAPFIELDUSER" == ""  ||
           "$ISPALDAPFIELDFIRST" == ""  ||  "$ISPALDAPFIELDLAST" == "" ||  "$ISPALDAPFIELDFULL" == ""  ||
           "$ISPALDAPFIELDEMAIL" == ""  ]] ; then
            echo "! Please set all LDAP options"
          elif [[ "$LDAPAUTO" == "" && "$ISPAPART" == "x" ]]; then
            echo "! Please set all LDAP Autoenroll options"
          elif [[ "$LDAPAUTO" == "true" && "$ISPAPART" == "x" ]] && [[ "$ISPALDAPCERTENROLL" == "" ]]; then
            echo "! Please set all LDAP Autoenroll options"
          elif [[ "$JAVAKEYSTOREPASSWD" == "" && "$ISPAPART" == "x" ]]; then
            echo "! Please set password for Java trust keystore"
          elif [[ "$P12PASSWD" == "" && "$ISPAPART" == "x" ]];then
            echo "! Please set password for admin P12 keystore"
          elif [[ "$SSLKEYSTOREPASSWD" == "" && "$ISPAPART" == "x" ]]; then
            echo "! Please set password for web server SSL keystore"
          elif [[ "$SYSTEMEJBCAPASSWD" == "" && "$EJBCAPART" == "x" ]]; then
            echo "! Please set password for ejbca system user"
          elif [[ "$SYSTEMISPAPASSWD" == "" && "$EJBCAPART" == "x" ]]; then
            echo "! Please set password for identear system user"
          elif [[ "$EJBCADBPASSWD" == "" && "$EJBCAPART" == "x" ]]; then
            echo "! Please set password for database user ejbca-user"
          elif [[ "$EJBCAFQN" == "" && "$EJBCAPART" == "x" ]]; then
            echo "! Please set FQDN for EJBCA "
          elif [[ "$ISPAPART" == "x" || "$EJBCAPART" == "x" ]]; then
            installSolution
          else
            echo "Error: Choose compontent which should be installed"
          fi


        ;;
        q)
          unset MAINLOOP
          exit 0
        ;;
        *)
         echo "--Invalid option--"
        ;;
      esac
      export PARTIALLOOP="show"
    done
}

main

exit 0
