'''
Created on 21 Jun 2017

@author: ChrisRussell
'''
import os



class menu(object):
    '''
    classdocs
    '''
       
    def __init__(self, title, values, options):
        '''
        Constructor
        '''
        self.title = title
        self.values = values
        self.options = options
        
    def showMenu(self):
        os.system('cls') 
        print(self.title)
        if self.values != None:
            for value in self.values:
                print(value.ljust(30) + self.values[value])
            
        optionNumber = 1
        for option in self.options:
            print(str(optionNumber) + ") " + option)
            optionNumber = optionNumber  + 1
        print("0) Exit")
        
    def getOption(self):
        return self.promptForResponse()

    def promptForResponse(self):
        answered = False
        while answered == False:
            
            self.showMenu()
            option = input("Enter option number ")
            try:
                optionAsInt = int(option)
            except ValueError:
                optionAsInt = -99
            
            if optionAsInt >= 0 and optionAsInt <= len(self.options):
                answered = True
                return optionAsInt - 1
            
    def askForNewValue(self, name, currentValue):
        option = input("Enter new value for " + name  + " (" + currentValue + ")")
        return option
    
        
            
class settingsMenu(object):
    def __init__(self, settings, title):
        '''
        Constructor
        '''
        self.title = title
        self.settings = settings
        
        
    def showMenu(self):
        os.system('cls') 
        print(self.title)
        
        optionNumber = 1
        for setting in self.settings:
            print(str(optionNumber) + ") " + setting + "\t" + self.settings[setting])
            optionNumber = optionNumber  + 1
        print("0) Exit")
        
    def promptForResponse(self):
        answered = False
        while answered == False:
            
            self.showMenu()
            option = input("Enter option number")
            try:
                optionAsInt = int(option)
            except ValueError:
                optionAsInt = -1
            
            if optionAsInt > 0 and optionAsInt <= len(self.options):
                answered = True
                return optionAsInt
        