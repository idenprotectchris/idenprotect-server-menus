'''
Created on 21 Jun 2017

@author: ChrisRussell
'''
from views import menu
from manager.DbManager import DbManager
from manager.CAManager import CAManager
from manager.EmailManager import EmailManager
from manager.LdapManager import LdapManager
from manager.ServerManager import ServerManager




if __name__ == '__main__':
    optionChosen = ""
    configRoot = "/etc/idenprotect"
    while optionChosen != -1:

        mainOptions = ['Certificate Authority','Database ', 'Email', 'LDAP', 'Server']
        mainMenu = menu.menu("Configuration Menu" , None, mainOptions)
        optionChosen = mainMenu.getOption()
        if optionChosen == 0:
            certManager = CAManager( configRoot +  "/ca.properties")
            certManager.getNewSettings()
        elif optionChosen == 1:
            dbManager = DbManager(configRoot + '/database.properties')
            dbManager.configureDb()    
        elif optionChosen == 2:
            emailManager = EmailManager(configRoot + '/email.properties')
            emailManager.configureEmailGateway()
        elif optionChosen == 3:
            ldapManager = LdapManager(configRoot + '/ldap.properties')
            ldapManager.getNewSettings()
        elif optionChosen == 4:
            serverManager = ServerManager(configRoot + '/webserver.properties')
            serverManager.configureServer()
               
             
    print("BYE")
                
            