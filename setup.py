'''
Created on 1 Aug 2017

@author: ChrisRussell
'''

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding

setup(
    name='wizard',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='1.0',

    description='Server set up wizard',
    long_description='for idenprotect server',
    packages=['manager', 'views'],
    entry_points={
        'console_scripts': [
            'wizard = wizard.iden:main_func',
        ]
    }
    )

    