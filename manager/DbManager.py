from manager.Manager import Manager
from views import menu
import configparser

class DbManager(Manager):
    '''
    classdocs
    '''
    dbSection = 'DATABASE'
    dbUsernameKey = 'username'
    dbPasswordKey = 'password'
    dbDriverKey = 'jdbc.driver'
    dbUrlKey = 'jdbc.url'
    dbChangeLogKey = 'change.log'
    prefix = "db."
    

    def __init__(self, params):
        super(DbManager, self).__init__(params)
        self.sectionName = self.dbSection
        
    
    def configureDb(self):
        
            
        self.sectionName = self.dbSection
        self.getSettings()
        shortSettings = self.removePrefixFromSettings(self.prefix , self.settings)
        settings = menu.menu("Database Settings" , shortSettings, shortSettings.keys())
        item = 0
        while item != -1:
            self.getSettings()
            shortSettings = self.removePrefixFromSettings(self.prefix , self.settings)
            settings = menu.menu("Database Settings" , shortSettings, shortSettings.keys())
            item = settings.getOption()
            if item != -1:
                toChangeList = list(shortSettings.keys())
                itemToChange = toChangeList[item]
                newvalue = settings.askForNewValue(itemToChange, shortSettings[itemToChange])
                    
                self.settings[self.prefix + itemToChange] = newvalue
                self.setSettings(self.settings)
             
        
    
    def splitUrl(self, temp):
        url = temp(self.dbUrlKey)
        url = self.removePrefix('jdbc:mysql://', url)
        host = url.split(':')[0]
        port = (url.split(':')[1]).split('/')[0]
        temp.delete(self.dbUrlKey);
        temp['host'] = host
        temp['port'] = port
        return temp
    
    def getSettings(self):
        Manager.getSettings(self)
        url = self.settings[self.prefix + self.dbUrlKey]
        url = self.removePrefix('jdbc:mysql://', url)
        host = url.split(':')[0]
        port = url.split(':')[1]
        port = port.split('/')[0]
        self.settings.pop(self.prefix + self.dbUrlKey)
        self.settings[self.prefix + 'host'] = host
        self.settings[self.prefix + 'port'] = port
        
    def setSettings(self, settings):
        url = 'jdbc:mysql://' + settings[self.prefix + 'host'] + ":" + settings[self.prefix + 'port'] + '/IDENTEAR?autoReconnect=true'
        settings[self.prefix + self.dbUrlKey] = url
        settings.pop(self.prefix + 'host')
        settings.pop(self.prefix + 'port')
        Manager.setSettings(self, settings)
    
        
