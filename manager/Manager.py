'''
Created on 7 Jul 2017

@author: ChrisRussell
'''
import configparser

class Manager(object):
    '''
    classdocs
    '''


    def __init__(self, filename):
        self.filename = filename
        self.settings = {}
        
    def getSettings(self):
        self.config = configparser.RawConfigParser()
        self.config.read(self.filename)
        self.settings = dict(self.config.items(self.sectionName))
        return self.settings
    
    def setSettings(self, settings):
        
        for key in settings:
            self.config.set(self.sectionName, key, settings[key])
        
        configfile = open( self.filename,'w')
        self.config.write(configfile)
     
    def removePrefix(self,prefix, string):
        stringToReturn = string[len(prefix) :]
        return stringToReturn
    
    def removePrefixFromSettings(self, prefix, settings):
        newsettings = {}
        
        for key in settings:
            newkey = self.removePrefix(prefix, key)
            newsettings[newkey] = settings[key]
        return newsettings
        