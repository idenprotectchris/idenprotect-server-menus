'''
Created on 19 Jul 2017

@author: ChrisRussell
'''
from manager.Manager import Manager
from views import menu

class ServerManager(Manager):
    '''
    classdocs
    '''
    serverSection = 'SERVER'
    serverPortKey = 'port'
    serverSslKeyStore = 'ssl.key-store'
    serverSslKeyStorePassword = 'ssl.key-store-password'
    serverSslKeyStoreType = 'ssl.keyStoreType'
    serverSslKeyAlias = 'ssl.keyAlias'
    
    prefix = "server."

    def __init__(self, params):
        super(ServerManager, self).__init__(params)
        self.sectionName = self.serverSection
        
    def configureServer(self):
                    
        self.sectionName = self.serverSection
        
        item = 0
        while item != -1:
            self.getSettings()
            shortSettings = self.removePrefixFromSettings(self.prefix , self.settings)
            settings = menu.menu("Server Settings" , shortSettings, shortSettings.keys())
            item = settings.getOption()
            if item != -1:
                toChangeList = list(shortSettings.keys())
                itemToChange = toChangeList[item]
                newvalue = settings.askForNewValue(itemToChange, shortSettings[itemToChange])
                    
                self.settings[self.prefix + itemToChange] = newvalue
                self.setSettings(self.settings)
             
        
        