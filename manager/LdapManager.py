'''
Created on 19 Jul 2017

@author: ChrisRussell
'''

from manager.Manager import Manager

from views import menu
import configparser

class LdapManager(Manager):
    
    serverSection = 'LDAPSERVER'  
    mappingSection = 'LDAPMAPPING'
    enrollSection = 'LDAPAUTOENROLL'  
    
    
    def __init__(self, params):
        super(LdapManager, self).__init__(params)
        self.sectionName = self.serverSection
        self.ldapOptions = ['Configure Server', 'Configure Mapping', 'Configure AutoEnroll']
       
    def getNewSettings(self):    
        
        option = 0
        while option != -1:
            settingsMenu = menu.menu("LDAP Main Menu", None , self.ldapOptions)
            option = settingsMenu.getOption()
            if option == 0:
                self.configureServer()
               
            elif option == 1:
                self.configureMapping()
                
            elif option == 2:
                self.configureEnroll()
                
    def configureServer(self):
        self.sectionName = self.serverSection
        self.prefix = "ldap."
        item = 99
        while item != -1:
            self.getSettings()
            shortSettings = self.removePrefixFromSettings(self.prefix , self.settings)
            ldapSettings = menu.menu("LDAP Server Settings" , shortSettings, shortSettings.keys())
            item = ldapSettings.getOption()
            if item != -1:
                toChangeList = list(shortSettings.keys())
                itemToChange = toChangeList[item]
                newvalue = ldapSettings.askForNewValue(itemToChange, shortSettings[itemToChange])
                    
                self.settings[self.prefix + itemToChange] = newvalue
                self.setSettings(self.settings)
    
    def configureMapping(self):
        self.sectionName = self.mappingSection
        self.prefix = "ldap.field."
        self.getSettings()
        shortSettings = self.removePrefixFromSettings(self.prefix , self.settings)
        ldapSettings = menu.menu("LDAP Mapping Settings" , shortSettings, shortSettings.keys())
        item = ldapSettings.getOption()
        if item != -1:
            toChangeList = list(shortSettings.keys())
            itemToChange = toChangeList[item]
            newvalue = ldapSettings.askForNewValue(itemToChange, shortSettings[itemToChange])
                    
            self.settings[self.prefix + itemToChange] = newvalue
            self.setSettings(self.settings)
            
    def configureEnroll(self):
        self.sectionName = self.enrollSection
        self.prefix = "ldap.autoenroll."
        self.getSettings()
        shortSettings = self.removePrefixFromSettings(self.prefix , self.settings)
        ldapSettings = menu.menu("LDAP Mapping Settings" , shortSettings, shortSettings.keys())
        item = ldapSettings.getOption()
        if item != -1:
            toChangeList = list(shortSettings.keys())
            itemToChange = toChangeList[item]
            newvalue = ldapSettings.askForNewValue(itemToChange, shortSettings[itemToChange])
                    
            self.settings[self.prefix + itemToChange] = newvalue
            self.setSettings(self.settings)   
    