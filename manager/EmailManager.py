'''
Created on 19 Jul 2017

@author: ChrisRussell
'''
from manager.Manager import Manager
from views import menu

class EmailManager(Manager):
    '''
    classdocs
    '''
    emailSection = 'EMAILGATEWAY'
    emailUsernameKey = 'username'
    emailPasswordKey = 'password'
    emailHostKey = 'host'
    emailPortKey = 'port'
    emailFromKey = 'from'
    emailDisplayNameKey = 'displayName'
   
    prefix = "email.gateway."

    def __init__(self, params):
        super(EmailManager, self).__init__(params)
        self.sectionName = self.emailSection
        
    def configureEmailGateway(self):
                    
        self.sectionName = self.emailSection
        
        item = 0
        while item != -1:
            self.getSettings()
            shortSettings = self.removePrefixFromSettings(self.prefix , self.settings)
            settings = menu.menu("Email Settings" , shortSettings, shortSettings.keys())
            item = settings.getOption()
            if item != -1:
                toChangeList = list(shortSettings.keys())
                itemToChange = toChangeList[item]
                newvalue = settings.askForNewValue(itemToChange, shortSettings[itemToChange])
                    
                self.settings[self.prefix + itemToChange] = newvalue
                self.setSettings(self.settings)
             
        
        