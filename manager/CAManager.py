'''
Created on 10 Jul 2017

@author: ChrisRussell
'''
from manager.Manager import Manager

from views import menu
import configparser

class CAManager(Manager):
    local = 'local'
    ejcba = 'ejbca'
    microscoft = "microsoft"
    casection = 'CA'
    ejbcasection = 'EJBCA'
    server = 'ca.backend'
    caname = 'ca.name'
    
    def __init__(self, params):
        super(CAManager, self).__init__(params)
        self.sectionName = self.casection
        self.caOptions = ['Change CA', 'Configure CA', 'Rename CA']
       
    def getNewSettings(self):    
        
        option = 0
        while option != -1:
            settings = menu.menu("CA Main Menu", self.getCaType(), self.caOptions)
            option = settings.getOption()
            if option == 0:
                settings = menu.menu("CA Main Menu", self.getCaType(), [self.local, self.ejcba, self.microscoft])
                caoption = settings.getOption()
                if caoption == 0:
                    self.setCaType(self.local)                                                    
                if caoption == 1:
                    self.setCaType(self.ejcba)
                if caoption == 2:
                    self.setCaType(self.microscoft)
               
            if option == 1:
                self.configureCa()
                
    
    def getCaType(self):
        self.sectionName = self.casection
        self.getSettings()
        
        return {self.server: self.settings[self.server], self.caname : self.settings[self.caname]}
    
    
    def setCaType(self, newServer):
        self.sectionName = self.casection
        self.settings[self.server] = newServer
        self.setSettings(self.settings)
        
    def configureCa(self):
        if(self.settings[self.server] == self.ejcba):
            prefix = 'ca.backend.ejbca.'
            self.sectionName = self.ejbcasection
            self.getSettings()
            shortSettings = self.removePrefixFromSettings( prefix , self.settings)
            settings = menu.menu("EJBCA Settings" , shortSettings, shortSettings.keys())
            item = 0
            while item != -1:
                item = settings.getOption()
                if item != -1:
                    toChangeList = list(shortSettings.keys())
                    itemToChange = toChangeList[item]
                    newvalue = settings.askForNewValue(itemToChange, shortSettings[itemToChange])
                    self.settings[prefix + itemToChange] = newvalue
                    self.setSettings(self.settings)
             
        